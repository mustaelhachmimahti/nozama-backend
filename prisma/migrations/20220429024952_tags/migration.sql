-- DropForeignKey
ALTER TABLE "ProductsOnTags" DROP CONSTRAINT "ProductsOnTags_tagId_fkey";

-- AddForeignKey
ALTER TABLE "ProductsOnTags" ADD CONSTRAINT "ProductsOnTags_tagId_fkey" FOREIGN KEY ("tagId") REFERENCES "Tag"("id") ON DELETE CASCADE ON UPDATE CASCADE;
