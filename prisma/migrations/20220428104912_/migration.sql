/*
  Warnings:

  - You are about to drop the column `tagId` on the `Product` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Product" DROP CONSTRAINT "Product_tagId_fkey";

-- AlterTable
ALTER TABLE "Product" DROP COLUMN "tagId";

-- CreateTable
CREATE TABLE "ProductsOnTags" (
    "productId" INTEGER NOT NULL,
    "tagId" INTEGER NOT NULL,
    "assignedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "ProductsOnTags_pkey" PRIMARY KEY ("productId","tagId")
);

-- AddForeignKey
ALTER TABLE "ProductsOnTags" ADD CONSTRAINT "ProductsOnTags_productId_fkey" FOREIGN KEY ("productId") REFERENCES "Product"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ProductsOnTags" ADD CONSTRAINT "ProductsOnTags_tagId_fkey" FOREIGN KEY ("tagId") REFERENCES "Tag"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
