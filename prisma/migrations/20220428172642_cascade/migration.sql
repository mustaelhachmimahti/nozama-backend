-- DropForeignKey
ALTER TABLE "ProductsOnTags" DROP CONSTRAINT "ProductsOnTags_productId_fkey";

-- AddForeignKey
ALTER TABLE "ProductsOnTags" ADD CONSTRAINT "ProductsOnTags_productId_fkey" FOREIGN KEY ("productId") REFERENCES "Product"("id") ON DELETE CASCADE ON UPDATE CASCADE;
