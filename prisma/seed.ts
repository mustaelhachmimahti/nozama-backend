import { PrismaClient } from '@prisma/client';
import { faker } from '@faker-js/faker';
import * as bcrypt from 'bcrypt';
import { ProductDto } from '../src/product/dto';

const prisma = new PrismaClient();

const fakerUser = async (): Promise<any> => ({
  email: faker.internet.email(),
  password: await bcrypt.hash(faker.internet.password(), 12),
  firstname: faker.name.firstName(),
  lastname: faker.name.lastName(),
  address: faker.address.streetAddress(true),
});

const adminUser = async (): Promise<any> => ({
  email: 'admin@admin.com',
  password: await bcrypt.hash('adminadmin', 12),
  firstname: 'admin',
  lastname: 'admin',
  address: 'admin',
  role: 'ADMIN',
});

const fakerProduct = (): ProductDto => ({
  title: faker.commerce.productName(),
  description: faker.commerce.productDescription(),
  imageLink: faker.image.imageUrl(),
  price: +faker.commerce.price(),
  stock: faker.random.number({ min: 0, max: 50 }),
});

async function main() {
  const fakerRounds = 50;
  console.log('Seeding...');
  /// --------- Seeds ---------------
  await prisma.user.create({ data: await adminUser() });
  for (let i = 0; i < fakerRounds; i++) {
    await prisma.user.create({ data: await fakerUser() });
    await prisma.product.create({ data: fakerProduct() });
    await prisma.tag.create({ data: { tagName: faker.commerce.department() } });
  }
}

main()
  .catch((err) => console.error(err))
  .finally(async () => {
    await prisma.$disconnect();
  });
