# Nozama

## Get started

1. `yarn` to install dependencies


2. Si vous voulez développer/tester, je vous recommande d'utiliser la base de données locale avec docker. Pour ce faire,
   vous devez installer docker


3. Quand vous avez installé et démarré Docker, faites `yarn db:dev:up`, pour connecter docker-composer avec postgres à
   docker


4. `yarn prisma:dev:deploy`


5. Une fois que tu as fait tout ce qui précède, tu peux utiliser des seeds et remplir les tableaux avec des données
   factices pour faciliter l'expérience de développement, avec `yarn seed`. Dans les seed, vous trouverez l'utilisateur
   admin qui a accès au backoffice.


6. `yarn dev` to run in mode development


7. Si tu lances la commande `npx prisma studio`, tu auras une interface de table, dans laquelle tu peux voir et gérer ta base de données.


8. Tu n'as pas à t'inquiéter des fichiers .env, car je les ai versionnés dans git lab. Assure-toi simplement que
   le `SECRET_JWT_TOKEN` est le même dans le frontend et le backend.

### Lien vers l'application déployée:

#### Lorsque vous vous rendez sur le site web et que vous essayez de vous connecter pour la première fois, il se peut qu'il n'y ait aucune réaction pendant 10 à 20 secondes. Cela ne se produit que la première fois, car il s'agit d'un service gratuit.

https://nozama-app.herokuapp.com/



##### Admin credential in deployed page

Admin email: admin@admin.com

Admin password: adminadmin
