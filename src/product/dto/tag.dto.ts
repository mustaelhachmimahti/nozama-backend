import { IsNotEmpty, IsNumber } from 'class-validator';

export class TagProduct {
  @IsNumber()
  @IsNotEmpty()
  tagId: number;
  @IsNumber()
  @IsNotEmpty()
  productId: number;
}
