import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { ProductDto, TagProduct } from './dto';

@Injectable()
export class ProductService {
  constructor(private prisma: PrismaService) {}

  async getAllProducts(): Promise<any[]> {
    try {
      return await this.prisma.product.findMany();
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async getOneProduct(id: number): Promise<any> {
    try {
      return await this.prisma.product.findUnique({
        where: {
          id,
        },
        include: { tags: true },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async addProduct(productDto: ProductDto): Promise<any> {
    try {
      return await this.prisma.product.create({
        data: {
          title: productDto.title,
          description: productDto.description,
          imageLink: productDto.imageLink,
          price: productDto.price,
          stock: productDto.stock,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async deleteProduct(id: number): Promise<any> {
    try {
      return await this.prisma.product.delete({
        where: {
          id,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async updateProduct(id: number, productDto: ProductDto): Promise<any> {
    try {
      return await this.prisma.product.update({
        where: {
          id: id,
        },
        data: {
          title: productDto.title,
          description: productDto.description,
          imageLink: productDto.imageLink,
          price: productDto.price,
          stock: productDto.stock,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async getTags(): Promise<any> {
    try {
      return await this.prisma.tag.findMany();
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async tagProductRelation(tagProduct: TagProduct[]): Promise<any> {
    try {
      return await this.prisma.productsOnTags.createMany({
        data: tagProduct,
        skipDuplicates: true,
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async matchProduct(title: string) {
    try {
      return await this.prisma
        .$queryRaw`SELECT * FROM "Product" WHERE title LIKE ${`%${title}%`}`;
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async deleteTag(id: number): Promise<any> {
    try {
      return await this.prisma.tag.delete({
        where: {
          id,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async addTag(tagName: string): Promise<any> {
    try {
      return await this.prisma.tag.create({
        data: {
          tagName: tagName,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async updateTag(id: number, tagName: string): Promise<any> {
    try {
      return await this.prisma.tag.update({
        where: {
          id,
        },
        data: {
          tagName: tagName,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }

  async getOneTag(id: number) {
    try {
      return await this.prisma.tag.findUnique({
        where: {
          id,
        },
      });
    } catch (err: any) {
      throw new ForbiddenException('Something went wrong');
    }
  }
}
