import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import { JwtGuard } from '../auth/guards';
import { ProductService } from './product.service';
import { Response } from 'express';
import { ProductDto, TagProduct } from './dto';

@UseGuards(JwtGuard)
@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @HttpCode(HttpStatus.OK)
  @Get()
  async getAllProducts(@Res() res: Response): Promise<void> {
    const response = await this.productService.getAllProducts();
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Post()
  async addProduct(
    @Body() productDto: ProductDto,
    @Res() res: Response,
  ): Promise<void> {
    const response = await this.productService.addProduct(productDto);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Post('/match/search')
  async matchProduct(
    @Body() title: { title: string },
    @Res() res: Response,
  ): Promise<void> {
    const response = await this.productService.matchProduct(title.title);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Delete('/remove/:id')
  async deleteProduct(@Res() res: Response, @Param('id') id: number) {
    const response = await this.productService.deleteProduct(+id);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Patch('/update/:id')
  async updateProduct(
    @Body() productDto: ProductDto,
    @Res() res: Response,
    @Param('id') id: number,
  ) {
    const response = await this.productService.updateProduct(+id, productDto);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Delete('/remove/tag/:id')
  async deleteTag(@Res() res: Response, @Param('id') id: number) {
    const response = await this.productService.deleteTag(+id);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Get('/:id')
  async getOneProduct(
    @Res() res: Response,
    @Param('id') id: number,
  ): Promise<void> {
    const response = await this.productService.getOneProduct(+id);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Get('/tag/all')
  async getTags(@Res() res: Response): Promise<void> {
    const response = await this.productService.getTags();
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Get('/tag/:id')
  async getOneTag(
    @Res() res: Response,
    @Param('id') id: number,
  ): Promise<void> {
    const response = await this.productService.getOneTag(+id);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Post('/tag/add')
  async addTag(
    @Res() res: Response,
    @Body() tag: { tag: string },
  ): Promise<void> {
    const response = await this.productService.addTag(tag.tag);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Patch('/tag/update/:id')
  async updateTag(
    @Res() res: Response,
    @Body() tag: { tag: string },
    @Param('id') id: number,
  ): Promise<void> {
    const response = await this.productService.updateTag(+id, tag.tag);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Post('tag-product')
  async tagProductRelationAdd(
    @Body() tagProduct: TagProduct[],
    @Res() res: Response,
  ) {
    console.log(tagProduct);
    const response = await this.productService.tagProductRelation(tagProduct);
    res.send({ response });
  }
}
