import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthLoginDto, AuthSignupDto } from './dto';
import { AuthService } from './auth.service';
import { Request, Response } from 'express';
import { JwtGuard } from './guards';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(JwtGuard)
  @HttpCode(HttpStatus.OK)
  @Get('payload')
  payload(@Req() req: Request, @Res() res: Response): any {
    const payload = req.user;
    res.send({ payload });
  }

  @HttpCode(HttpStatus.OK)
  @Post('login')
  async login(@Body() authDto: AuthLoginDto, @Res() res: Response) {
    const response = await this.authService.login(authDto);
    res.send({ response });
  }

  @HttpCode(HttpStatus.OK)
  @Post('signup')
  async signup(@Body() authDto: AuthSignupDto, @Res() res: Response) {
    const response = await this.authService.signup(authDto);
    res.send(response);
  }
}
