import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { AccessToken, AuthLoginDto, AuthSignupDto } from './dto';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService, private jwt: JwtService) {}

  async signup(dto: AuthSignupDto): Promise<any> {
    const hash = await bcrypt.hash(dto.password, 12);
    try {
      const user = await this.prisma.user.create({
        data: {
          email: dto.email,
          password: hash,
          firstname: dto.firstName,
          lastname: dto.lastName,
          address: dto.address,
        },
      });
      delete user.password;
      return user;
    } catch (err) {
      if (err instanceof PrismaClientKnownRequestError) {
        if (err.code === 'P2002') {
          throw new ForbiddenException('Credentials taken');
        }
      }
    }
  }

  async login(dto: AuthLoginDto): Promise<any> {
    const user = await this.prisma.user.findUnique({
      where: {
        email: dto.email,
      },
    });

    if (!user) throw new ForbiddenException('Invalid credential');
    const isMatch = await bcrypt.compare(dto.password, user.password);
    if (!isMatch) throw new ForbiddenException('Invalid credential');
    delete user.password;

    return this.creationAccessToken(
      user.id,
      user.email,
      user.firstname,
      user.lastname,
      user.role,
      user.address,
      user.createdAt,
      user.updatedAt,
    );
  }

  async creationAccessToken(
    userId: number,
    email: string,
    firstName: string,
    lastName: string,
    role: string,
    address: string,
    createdAt: Date,
    updatedAt: Date,
  ): Promise<AccessToken> {
    const payload = {
      sub: userId,
      email,
      firstName,
      lastName,
      role,
      address,
      createdAt,
      updatedAt,
    };

    const accessToken: string = await this.jwt.signAsync(payload, {
      expiresIn: '1h',
      secret: process.env.SECRET_JWT_TOKEN,
    });

    return { accessToken };
  }
}
